import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LaskukoneTest {

	@Test // testit jokaiselle Laskukone -luokan metodille
	void lisääLukuTest() {
		Laskukone kone = new Laskukone();
		kone.lisaaLuku(5);
		assertTrue(kone.annaTulos() == 6);
	}
	void vahennaLukuTest() {
		Laskukone kone = new Laskukone();
		kone.vahennaLuku(5);
		assertTrue(kone.annaTulos() == -4);
	}
	void kerroLuvullaTest() {
		Laskukone kone = new Laskukone();
		kone.kerroLuvulla(2);
		assertTrue(kone.annaTulos() == 2);
	}
	void korotaPotenssiinTest() {
		Laskukone kone = new Laskukone();
		kone.korotaPotenssiin(2);
		assertTrue(kone.annaTulos() == 1);
	}
	void annaTulosTest() {
		Laskukone kone = new Laskukone();
		assertTrue(kone.annaTulos() == 1);
	}
	void nollaaTest() {
		Laskukone kone = new Laskukone();
		kone.nollaa();
		assertTrue(kone.annaTulos() == 1);
	}


}
